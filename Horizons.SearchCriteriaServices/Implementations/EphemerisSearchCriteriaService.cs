﻿using Horizons.Common.Consts;
using Horizons.Common.Enums;
using Horizons.Common.Queries;
using Horizons.Resources.QueryString.Name;
using Horizons.Resources.QueryString.Value;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Horizons.SearchCriteriaServices.Implementations
{
    public class EphemerisSearchCriteriaService : IEphemerisSearchCriteriaService
    {
        string IEphemerisSearchCriteriaService.BuildQueryString(EphemerisSearchCriteria ephemerisSearchCriteria)
        {
            var QueryParamsDictionary = new Dictionary<string, string>();
            QueryParamsDictionary.Add(QueryStringNameResource.Batch, "1");
            QueryParamsDictionary.Add(QueryStringNameResource.TList, QuoteParam(ToJulianDate(ephemerisSearchCriteria.Date).ToString(CultureInfo.InvariantCulture)));
            QueryParamsDictionary.Add(QueryStringNameResource.Command, QuoteParam(((long)ephemerisSearchCriteria.SolarSystemElementType).ToString()));

            QueryParamsDictionary.Add(QueryStringNameResource.Center, QuoteParam($"@{(int)SolarSystemElementType.SolarSystemBarycenter}"));
            QueryParamsDictionary.Add(QueryStringNameResource.CSVFormat, QuoteParam(BinaryValueResource.Yes));
            QueryParamsDictionary.Add(QueryStringNameResource.MakeEphemeris, QuoteParam(BinaryValueResource.Yes));
            QueryParamsDictionary.Add(QueryStringNameResource.ObjectData, QuoteParam(BinaryValueResource.No));
            QueryParamsDictionary.Add(QueryStringNameResource.OutputUnits, QuoteParam(OutputUnitResource.Kilometers));
            QueryParamsDictionary.Add(QueryStringNameResource.RangeUnits, QuoteParam(RangeUnitResource.KilometersPerDay));
            QueryParamsDictionary.Add(QueryStringNameResource.TableType, QuoteParam(TableTypeResource.Vectors));
            QueryParamsDictionary.Add(QueryStringNameResource.VectorLabels, QuoteParam(BinaryValueResource.No));
            QueryParamsDictionary.Add(QueryStringNameResource.VectorTable, QuoteParam(((int)VectorTableType.StateVectors).ToString()));

            return QueryParamsDictionary.Aggregate("?", (queryString, queryParameters) => queryString += $"{queryParameters.Key}={queryParameters.Value}&");
        }
        
        private string QuoteParam(string param)
        {
            return $"'{param}'";
        }

        private double ToJulianDate(DateTime date)
        {
            return date.ToOADate() + HorizonConsts.JulianToOAOffset;
        }
    }
}
