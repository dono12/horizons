﻿using Autofac;

namespace Horizons.Handlers
{
    public class SearchCriteriaServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(SearchCriteriaServiceModule).Assembly)
                .Where(x => x.Name.EndsWith("Service"))
                .AsImplementedInterfaces();
            
        }
    }
}
