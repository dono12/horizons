﻿using Horizons.Common.Queries;
using System.Collections.Generic;

namespace Horizons.SearchCriteriaServices.Implementations
{
    public interface IEphemerisSearchCriteriaService
    {
        string BuildQueryString(EphemerisSearchCriteria ephemerisSearchCriteria);
    }
}
