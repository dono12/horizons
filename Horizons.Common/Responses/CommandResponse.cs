﻿namespace Horizons.Common.Responses
{
    public class CommandResponse<T> : CommandResponse
    {
        public T Payload { get; set; }
    }

    public class CommandResponse : BaseResponse
    {
    }
}
