﻿namespace Horizons.Common.Responses
{
    public class QueryResponse<T> : QueryResponse
    {
        public T Payload { get; set; }
    }

    public class QueryResponse : BaseResponse
    {
    }
}
