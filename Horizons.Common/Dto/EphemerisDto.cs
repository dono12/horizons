﻿using Horizons.Common.Enums;
using System;

namespace Horizons.Common.Dto
{
    public class EphemerisDto
    {
        public SolarSystemElementType SolarSystemElementType { get; set; }
        public DateTime EpehemrisDate { get; set; }
        public double X {get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        public double VX { get; set; }
        public double VY { get; set; }
        public double VZ { get; set; }
    }
}
