﻿namespace Horizons.Common
{
    public class HorizonsConfiguration
    {
        public string Url { get; set; }
        public int Port { get; set; }
        public string DateFormat { get; set; }
    }
}
