﻿namespace Horizons.Common.Enums
{
    public enum VectorTableType
    {
        PositionComponents = 1,
        StateVectors = 2
    }
}
