﻿namespace Horizons.Common.Enums
{
    public enum OutputUnitType
    {
        KilometersPerSecond,
        KilometersPerDay,
        AstronomicalUnitsPerDay,

    }
}
