﻿namespace Horizons.Common.Enums
{
    public enum SolarSystemElementType
    {
        SolarSystemBarycenter = 0,

        #region Planets

        Sun = 10,
        Mercury = 199,
        Venus = 299,

        #region Earth

        Moon = 301,
        Earth = 399,

        #endregion

        Mars = 499,
        Jupiter = 599,
        Saturn = 699,
        Uranus = 799,
        Neptune = 899,
        Pluto = 999,

        #endregion
    }
}
