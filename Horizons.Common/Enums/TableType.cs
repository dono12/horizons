﻿namespace Horizons.Common.Enums
{
    public enum TableType
    {
        Observer,
        Elements,
        Vectors,
        Approach
    }
}
