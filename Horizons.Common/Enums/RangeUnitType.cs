﻿namespace Horizons.Common.Enums
{
    public enum RangeUnitType
    {
        Kilometers,
        AstronomicalUnits
    }
}
