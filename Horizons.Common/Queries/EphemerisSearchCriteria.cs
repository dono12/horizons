﻿using Horizons.Common.Enums;
using System;

namespace Horizons.Common.Queries
{
    public class EphemerisSearchCriteria
    {
        public SolarSystemElementType SolarSystemElementType { get; set; }
        public DateTime Date { get; set; }
    }
}
