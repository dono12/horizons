﻿using System;

namespace Horizons.Common.Queries
{
    public class AllEphemerisSearchCriteria
    {
        public DateTime Date { get; set; }
    }
}
