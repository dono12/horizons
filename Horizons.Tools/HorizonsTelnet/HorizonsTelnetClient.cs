﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Horizons.Tools.HorizonsTelnet
{
    public class HorizonsTelnetClient
    {

        private TelnetClient TelnetClient { get; set; }

        public HorizonsTelnetClient(string url, int port)
        {
            TelnetClient = new TelnetClient(url, port);
            Setup().Wait();
        }

       
        
        async public Task<IEnumerable<string>> SendAsync(string data, char separator = ',')
        {
            var responses = new List<string>();
            var splittedInput = SplitInputData(data, separator);
            foreach(var input in splittedInput)
            {
                await TelnetClient.WriteAsync(input);
                responses.Add(await TelnetClient.ReadAsync());
            }
            return responses;
        }

        async public Task<string> ReadAsync()
        {
            return await TelnetClient.ReadAsync();
        }
        private IEnumerable<string> SplitInputData(string data, char separator)
        {
            return data.Split(separator);
        }

        async private Task Setup()
        {
            var response = await TelnetClient.ReadRawAsync();
            if (response.Length > 0)
            {
                var telnetRequests = ParseRequests(response);
                foreach (var telnetRequest in telnetRequests)
                {
                    switch (telnetRequest.Option)
                    {
                        case TelnetRequestOption.Echo:
                        case TelnetRequestOption.SuppressGoAhead:
                            await TelnetClient.WriteRawAsync(new byte[] { TelnetConsts.IAC, (byte)((int)telnetRequest.Command ^ 2), (byte)telnetRequest.Option });
                            break;
                        case TelnetRequestOption.NegotiateWindowSize:
                            await TelnetClient.WriteRawAsync(
                                    new byte[] { TelnetConsts.IAC,
                                    (byte)((int)telnetRequest.Command ^ 2),
                                    (int)TelnetRequestOption.Echo }
                                );
                            break;
                        case TelnetRequestOption.TerminalType:
                            break;
                            //default:
                            //    throw new NotImplementedException();
                    }
                }
            }
            else
            {
            }
        }

        private IEnumerable<TelnetRequest> ParseRequests(byte[] data)
        {
            var telnetRequests = new List<TelnetRequest>();
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] == TelnetConsts.IAC)
                {
                    var telnetRequest = new TelnetRequest();
                    telnetRequest.Command = (TelnetRequestCommand)data[i];
                    telnetRequest.Option = (TelnetRequestOption)data[i + 1];
                    telnetRequests.Add(telnetRequest);
                    i += 2;
                }
            }
            return telnetRequests;
        }

    }
}
