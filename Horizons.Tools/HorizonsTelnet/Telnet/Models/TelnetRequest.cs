﻿namespace Horizons.Tools.HorizonsTelnet
{
    class TelnetRequest
    {
        public TelnetRequestCommand Command { get;set; }
        public TelnetRequestOption Option { get; set; }
    }
}
