﻿namespace Horizons.Tools.HorizonsTelnet
{
    static class TelnetConsts
    {
        public const int IAC = 255;
        public static readonly string Enter = new string(new char[] { (char)13, (char)10 });
    }
}
