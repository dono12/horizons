﻿namespace Horizons.Tools.HorizonsTelnet
{
    enum TelnetRequestCommand
    {
        Will = 251,
        Wont = 252,
        Do = 253,
        Dont = 254
    }
}
