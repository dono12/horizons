﻿namespace Horizons.Tools.HorizonsTelnet
{
    enum TelnetRequestOption
    {
        BinaryTransmission = 0,
        Echo = 1,
        SuppressGoAhead = 3,
        TerminalType = 24,
        NegotiateWindowSize = 31,
    }
}
