﻿using System.Diagnostics;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Horizons.Tools.HorizonsTelnet
{
    class TelnetClient
    {
        private TcpClient TcpClient { get; set; }
        private NetworkStream NetworkStream => TcpClient.GetStream();

        public TelnetClient(string url, int port)
        {
            TcpClient = new TcpClient(url, port);
        }

        async public Task<string> ReadAsync()
        {
            return Encoding.ASCII.GetString(await ReadRawAsync());
        }

        async public Task<byte[]> ReadRawAsync()
        {
            await WaitForDataAsync();
            var buffer = new byte[TcpClient.Available];
            if (NetworkStream.CanRead && buffer.Length > 0)
            {
                await NetworkStream.ReadAsync(buffer, 0, buffer.Length);
            }
            return buffer;
        }

        async public Task WriteAsync(string data)
        {
            var bytes = Encoding.ASCII.GetBytes(data + TelnetConsts.Enter);
            await WriteRawAsync(bytes);
        }

        async public Task WriteRawAsync(byte[] data)
        {
            await NetworkStream.WriteAsync(data, 0, data.Length);
            await ReadAsync();
        }

      
        private Task WaitForDataAsync(long timeoutInMillis = 1000)
        {
            return Task.Run(() => {
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();
                while (!NetworkStream.DataAvailable && stopwatch.ElapsedMilliseconds < timeoutInMillis) ;
                stopwatch.Stop();
            });
        }

    }
}
