﻿using Horizons.Common.Dto;
using Horizons.Common.Queries;
using System.Threading.Tasks;

namespace Horizons.Handlers.Components.EphemerisComponent.Implementation
{
    public interface IEphemerisComponent
    {
        Task<EphemerisDto> GetEphemeris(EphemerisSearchCriteria searchCriteria);
    }
}
