﻿using Autofac;
using Horizons.Common;
using Horizons.Common.Dto;
using Horizons.Common.Queries;
using Horizons.Resources.Ephemeris;
using Horizons.SearchCriteriaServices.Implementations;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Horizons.Handlers.Components.EphemerisComponent.Implementation
{
    public class EphemerisComponent : IEphemerisComponent
    {
        private IOptions<HorizonsConfiguration> HorizonsConfigurationOptions { get; set; }
        private HttpClient HttpClient { get; set; }
        private IEphemerisSearchCriteriaService EphemerisSearchCriteriaService { get; set; }
        public EphemerisComponent(IComponentContext componentContext)
        {
            HorizonsConfigurationOptions = componentContext.Resolve<IOptions<HorizonsConfiguration>>();
            EphemerisSearchCriteriaService = componentContext.Resolve<IEphemerisSearchCriteriaService>();
            HttpClient = new HttpClient();
        }

        public HorizonsConfiguration HorizonsConfiguration => HorizonsConfigurationOptions.Value;

        async public Task<EphemerisDto> GetEphemeris(EphemerisSearchCriteria searchCriteria)
        {
            var queryString = EphemerisSearchCriteriaService.BuildQueryString(searchCriteria);

            var response = await HttpClient.GetAsync(HorizonsConfiguration.Url + queryString);
            var ephemerisDto = ParseEphemeris(await response.Content.ReadAsStringAsync());
            ephemerisDto.SolarSystemElementType = searchCriteria.SolarSystemElementType;
            return ephemerisDto;
        }

        public EphemerisDto ParseEphemeris(string rawEphemeris, char separator = ',')
        {
            var startOfEphemerisIndex = rawEphemeris.IndexOf(EphemerisResource.StartOfEphemerisSequence);
            if (startOfEphemerisIndex == -1)
            {
                return null;
            }
            startOfEphemerisIndex += EphemerisResource.StartOfEphemerisSequence.Length;
            var endOfEphemerisIndex = rawEphemeris.IndexOf(EphemerisResource.EndOfEphemerisSequence);
            var charCount = endOfEphemerisIndex - startOfEphemerisIndex;
            var ephemerisElements =
                rawEphemeris
                .Substring(startOfEphemerisIndex, charCount)
                .Replace("\n", "")
                .Split(separator);

            return new EphemerisDto()
            {
                EpehemrisDate = ParseDate(ephemerisElements[1]),
                X = ParseDouble(ephemerisElements[2]),
                Y = ParseDouble(ephemerisElements[3]),
                Z = ParseDouble(ephemerisElements[4]),
                VX = ParseDouble(ephemerisElements[5]),
                VY = ParseDouble(ephemerisElements[6]),
                VZ = ParseDouble(ephemerisElements[7])
            };

        }

        private double ParseDouble(string value)
        {
            return double.Parse(value, NumberStyles.Float, CultureInfo.InvariantCulture);
        }
        private DateTime ParseDate(string value)
        {
            return DateTime.Parse(value);
        }
    }
}
