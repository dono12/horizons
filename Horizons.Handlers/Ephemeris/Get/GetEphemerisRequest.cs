﻿using Horizons.Common.Dto;
using Horizons.Common.Queries;
using Horizons.Common.Responses;
using MediatR;

namespace Horizons.Requests.Ephemeris
{
    public class GetEphemerisRequest : IRequest<QueryResponse<EphemerisDto>>
    {
        public EphemerisSearchCriteria SearchCriteria { get; set; }
    }
}
