﻿using Autofac;
using Horizons.Common.Dto;
using Horizons.Common.Responses;
using Horizons.Handlers.Components.EphemerisComponent.Implementation;
using Horizons.Requests.Ephemeris;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
namespace Horizons.Handlers.Ephemeris
{
    public class GetEphemerisHandler : IRequestHandler<GetEphemerisRequest, QueryResponse<EphemerisDto>>
    {
        IEphemerisComponent EphemerisComponent { get; set; }
        public GetEphemerisHandler(IComponentContext componentContext)
        {
            EphemerisComponent = componentContext.Resolve<IEphemerisComponent>();
        }
        async public Task<QueryResponse<EphemerisDto>> Handle(GetEphemerisRequest request, CancellationToken cancellationToken)
        {
            return new QueryResponse<EphemerisDto>()
            {
                Payload = await EphemerisComponent.GetEphemeris(request.SearchCriteria)
            };
        }
    }
}

