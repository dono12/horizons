﻿using Horizons.Common.Dto;
using Horizons.Common.Queries;
using Horizons.Common.Responses;
using MediatR;
using System.Collections.Generic;

namespace Horizons.Requests.Ephemeris
{
    public class GetAllEphemerisRequest : IRequest<QueryResponse<List<EphemerisDto>>>
    {
        public AllEphemerisSearchCriteria SearchCriteria { get; set; }
    }
}
