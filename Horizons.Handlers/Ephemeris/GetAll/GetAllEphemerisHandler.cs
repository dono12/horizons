﻿using Autofac;
using Horizons.Common.Dto;
using Horizons.Common.Enums;
using Horizons.Common.Queries;
using Horizons.Common.Responses;
using Horizons.Handlers.Components.EphemerisComponent.Implementation;
using Horizons.Requests.Ephemeris;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Horizons.Handlers.Ephemeris
{
    public class GetAllEphemerisHandler : IRequestHandler<GetAllEphemerisRequest, QueryResponse<List<EphemerisDto>>>
    {
        IEphemerisComponent EphemerisComponent { get; set; }
        public GetAllEphemerisHandler(IComponentContext componentContext)
        {
            EphemerisComponent = componentContext.Resolve<IEphemerisComponent>();
        }
        async public Task<QueryResponse<List<EphemerisDto>>> Handle(GetAllEphemerisRequest request, CancellationToken cancellationToken)
        {
            var allEphemeris = new List<EphemerisDto>();
            var solarSystemElementTypes = Enum
                .GetValues(typeof(SolarSystemElementType))
                .Cast<SolarSystemElementType>()
                .Where(x => x != SolarSystemElementType.SolarSystemBarycenter);

            foreach (var solarSystemElementType in solarSystemElementTypes)
            {
                allEphemeris.Add(
                    await EphemerisComponent.GetEphemeris(
                        new EphemerisSearchCriteria()
                        {
                            Date = request.SearchCriteria.Date,
                            SolarSystemElementType = solarSystemElementType,
                        }
                    )
                );
            }

            return new QueryResponse<List<EphemerisDto>>()
            {
                Payload = allEphemeris
            };
        }
    }
}