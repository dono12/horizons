﻿using Autofac;

namespace Horizons.Handlers
{
    public class HandlersModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(HandlersModule).Assembly)
                .Where(x => x.Name.EndsWith("Handler"))
                .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(typeof(HandlersModule).Assembly)
                .Where(x => x.Name.EndsWith("Component"))
                .AsImplementedInterfaces();

        }
    }
}
