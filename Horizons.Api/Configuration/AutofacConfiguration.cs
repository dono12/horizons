﻿using Autofac;
using Horizons.Handlers;
using MediatR;

namespace Horizons.Api.Configuration
{
    public class AutofacConfiguration : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Mediator>()
                .As<IMediator>()
                .InstancePerLifetimeScope();
            builder.Register<ServiceFactory>(ctx =>
            {
                var c = ctx.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });
            builder.RegisterModule<HandlersModule>();
            builder.RegisterModule<SearchCriteriaServiceModule>();
        }
    }
}
