﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Horizons.Common.Dto;
using Horizons.Requests;
using Horizons.Requests.Ephemeris;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Horizons.Api.Controllers
{
    [Route("api/[controller]")]
    public class EphemerisController : BaseController
    {
        private IMediator mediator { get; set; }

        public EphemerisController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet]
        [Route("/api/[controller]")]
        public async Task<IActionResult> Get(GetEphemerisRequest request)
        {
            return await HandleAsync(async () => await mediator.Send(request));
        }


        [HttpGet]
        [Route("/api/[controller]/all")]
        public async Task<IActionResult> GetAll(GetAllEphemerisRequest request)
        {
            return await HandleAsync(async () => await mediator.Send(request));
        }
    }
}
