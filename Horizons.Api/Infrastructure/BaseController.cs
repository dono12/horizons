﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Horizons.Common.Responses;
using Horizons.Requests;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Horizons.Api.Controllers
{
    public class BaseController : Controller
    {
        async public Task<IActionResult> HandleAsync<T>(Func<Task<QueryResponse<T>>> func)
        {
            var result = await func();

            return Ok(result);
        }
    }
}
