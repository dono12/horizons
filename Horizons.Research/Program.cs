﻿using Horizons.Tools.HorizonsTelnet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Horizons.Research
{
    class Program
    {
        static HorizonsTelnetClient HorizonsTelnetClient { get; set; }
        public static void Main(string[] args)
        {
                while (true)
                {
                    var input = Console.ReadKey().Key;
                    switch (input)
                    {
                        case ConsoleKey.D1:
                            Console.WriteLine("Option: Start");
                            Go().Wait();
                        break;
                        case ConsoleKey.D2:
                            Console.WriteLine("Option: Write");
                            Write(Console.ReadLine()).Wait();
                        break;
                        case ConsoleKey.D3:
                            Console.WriteLine("Option: Read");
                            Read().Wait();
                        break;
                    }
                }




        }
        
        async static public Task Go()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            HorizonsTelnetClient = new HorizonsTelnetClient("horizons.jpl.nasa.gov", 6775);
            Console.WriteLine(ToBytes(await HorizonsTelnetClient.ReadAsync()));
        }

        async static public Task Write(string data)
        {
            var responses = await HorizonsTelnetClient.SendAsync(data);
            foreach (var resposne in responses)
            {
                Console.WriteLine(resposne);
            }
        }
        async static public Task Read()
        {
            Console.Write(await HorizonsTelnetClient.ReadAsync());
        }

        static public string ToBytes(string data)
        {
            if (data.Length > 10)
            {
                return data;
            }
            return data.Select(x => x).Aggregate<char, string>("", (x, y) => x += $"{(int)y} ");
        }

        //await TelnetClient.WriteRawAsync(new byte[] { 255, 250, 31, 255, 255, 0, 24, 255, 240 });
        //await TelnetClient.WriteRawAsync(new byte[] { 255, 250, 24, 0, 86, 84, 49, 48, 50, 255, 240 });
    }
}
